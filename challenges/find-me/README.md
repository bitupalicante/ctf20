
# Find Me

## Puntos

* Puntuación Máxima `100`
* Puntuación Mínima `50`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{mobilehackingspace.org}`

## Adjuntos

[Find Me](files/FindMe.apk) (MD5: 83b38a8c71a46cd1daab178b38c3fbff)

## Deploy

Se dispone de proyecto Android Studio con código original en Java para generar el APK. Para ello descargar el fichero [FindMe.7z](deploy/FindMe.7z), descomprimir e importarlo.

## Descripcion

¿Serás capaz de encontrarme?

## Solucion

```
 _____ ______   ___  ___  ________                              ________ ___  ________   ________          _____ ______   _______      
|\   _ \  _   \|\  \|\  \|\   ____\                            |\  _____\\  \|\   ___  \|\   ___ \        |\   _ \  _   \|\  ___ \     
\ \  \\\__\ \  \ \  \\\  \ \  \___|_         ____________      \ \  \__/\ \  \ \  \\ \  \ \  \_|\ \       \ \  \\\__\ \  \ \   __/|    
 \ \  \\|__| \  \ \   __  \ \_____  \       |\____________\     \ \   __\\ \  \ \  \\ \  \ \  \ \\ \       \ \  \\|__| \  \ \  \_|/__  
  \ \  \    \ \  \ \  \ \  \|____|\  \      \|____________|      \ \  \_| \ \  \ \  \\ \  \ \  \_\\ \       \ \  \    \ \  \ \  \_|\ \ 
   \ \__\    \ \__\ \__\ \__\____\_\  \                           \ \__\   \ \__\ \__\\ \__\ \_______\       \ \__\    \ \__\ \_______\
    \|__|     \|__|\|__|\|__|\_________\                           \|__|    \|__|\|__| \|__|\|_______|        \|__|     \|__|\|_______|
                            \|_________|                                                                                               
                                                                                                                                       
```

Voy a comentaros un poco qué tools necesitamos para solucionar Find Me, un poco de background rápido, fake flag, tools, etc.
La idea de este challenge, es darle a la persona que va a resolverlo idea de alguno de los paths donde usualmente los devs dejan keys o credenciales.

Tipo de resolución: Este reto se resuelve de forma estática, es decir, sin necesidad de ejecutarlo en ningún dispositivo.

Herramientas a utilizar: [APKTOOL](https://ibotpeaches.github.io/Apktool/) Está disponible para Windows, Linux y Mac OS.  
URL de descarga: https://ibotpeaches.github.io/Apktool/install/

1. Realizamos la descarga e instalación de la tool
2. Ejecutamos `APKTOOL d FindMe.apk`

Las flags se encuentran en los siguientes directorios:

**Fake Flag**:
Se encuentra en el directorio `res\raw\values.xml` y al abrirlo, se observa la siguiente variable `<resources> flag = aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1kUXc0dzlXZ1hjUQ== </resources>` codificada en base64.

Base64 decode: https://www.youtube.com/watch?v=dQw4w9WgXcQ (Video Rick Astley)

**Flag**:
Se encuentra en el directorio `res\values\strings.xml` y al abrir el fichero, podremos observar la variable `<string name="fl4g">WW1sMGRYQXlNSHR0YjJKcGJHVm9ZV05yYVc1bmMzQmhZMlV1YjNKbmZRPT0=</string>` la cual se encuentra codificada dos veces en base64.

Primer decode: Yml0dXAyMHttb2JpbGVoYWNraW5nc3BhY2Uub3JnfQ==  
Segundo decode (Flag): bitup20{mobilehackingspace.org}

**Nota:** Si se ejecuta la aplicación dirá que el dispostivo está rooteado y aunque se hookee el método `mobile.hacking.space.MHS.RootUtil.isDeviceRooted` para el bypass, no sucederá nada. Solo es una distracción.

## Referencias

* https://ibotpeaches.github.io/Apktool/

