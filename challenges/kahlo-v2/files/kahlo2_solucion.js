Java.perform( function () {
	
	var LaCaja = Java.use("com.bitup.kahlo2.encryption.LaCaja");
	var LaCajaCompanion = Java.use("com.bitup.kahlo2.encryption.LaCaja$Companion");
	
	LaCajaCompanion.setValue.implementation = function (pos, val) {
		LaCaja.intArray.value[pos] = val;
	}

	LaCajaCompanion.getValue.implementation = function (pos) {
		return LaCaja.intArray.value[pos];
	}

	LaCajaCompanion.swap.implementation = function (r1, r2) {
		var temp = LaCaja.intArray.value[r1];
		LaCaja.intArray.value[r1] = LaCaja.intArray.value[r2];
		LaCaja.intArray.value[r2] = temp;	
	}

	
	LaCajaCompanion.swap.setearCajaEnCero = function () {
		for (var i = 0; i < 256; i++) {
			LaCaja.intArray.value[i] = 0;
		}		
	}

	var LaClave = Java.use("com.bitup.kahlo2.encryption.LaClave");
	
	LaClave.getLongitudBytes.implementation = function () {
		return 255;	
	}

	
	LaClave.setKey.implementation = function (arrayBytes) {
		this.key.value = arrayBytes;
	}

	var OfuscadorDividido = Java.use("com.bitup.kahlo2.encryption.OfuscadorDividido");
	var String = Java.use("java.lang.String");

	OfuscadorDividido.decryptMessage.overload('[B', 'java.nio.charset.Charset', 'java.lang.String').implementation = function (message,charset,key) {
		
		this.reset();
		this.setKey(key);
		var msg = this.crypt(message);
		this.reset();
		return String.$new(msg,charset);			
	}
		
	OfuscadorDividido.moduleOperation.implementation = function (r1, r2) {
		return r1 % r2;	
	}
});
