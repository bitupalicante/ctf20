from oauth2_provider.oauth2_validators import OAuth2Validator


class VulnerableOAuth2Validator(OAuth2Validator):
    def invalidate_authorization_code(
        self, client_id, code, request, *args, **kwargs
    ):

        pass
