from django.shortcuts import render, redirect
from django.views import View
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth
from django.conf import settings

import requests

from .models import Tokens, BankApp
from .utils import get_transactions

happy_bank = BankApp.objects.filter(name="HappyBank").first()
happy_bank.base_url = settings.HAPPY_BANK_URL
happy_bank.save()

if not happy_bank:
    raise Exception("HappyBank reord should exist in database")
if not happy_bank.base_url.endswith("/"):
    happy_bank.base_url += "/"


class HomeView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return redirect("profile")
        return render(request, "base.html")


class RegisterView(FormView):
    form_class = UserCreationForm
    success_url = "/"
    template_name = "login.html"

    def form_valid(self, form):
        u = User(username=form.cleaned_data["username"])
        u.set_password(form.cleaned_data["password1"])
        u.save()

        return super().form_valid(form)


class ProfileView(View):
    def get(self, request):
        tokens = Tokens.objects.filter(user=request.user).all()
        redirect_url = request.build_absolute_uri(reverse("grants"))

        transactions = []
        for token in tokens:
            transactions += get_transactions(token)

        connect_url = (
            happy_bank.base_url
            + f"oauth/authorize/?response_type=code&client_id={happy_bank.client_id}&redirect_url={redirect_url}&scopes=transactions&state=1234"
        )
        return render(
            request,
            "profile.html",
            dict(
                tokens=tokens,
                connect_url=connect_url,
                transactions=transactions,
            ),
        )


class GrantsView(View):
    def get(self, request):
        if request.GET.get("error"):
            messages.add_message(
                request, messages.ERROR, "Authorization fail"
            )
            return redirect("profile")

        if request.GET.get("code"):
            auth_response = requests.post(
                happy_bank.base_url + "oauth/token/",
                files={
                    "grant_type": (None, "authorization_code"),
                    "code": (None, request.GET["code"]),
                    "client_id": (None, happy_bank.client_id),
                    "client_secret": (None, happy_bank.client_secret),
                    "scopes": (None, "transactions"),
                },
            )
            json_response = auth_response.json()

            if "error" not in json_response:
                # Delete any previous Token for the user
                Tokens.objects.filter(user=request.user).delete()
                # Store retrieved access token
                Tokens.objects.create(
                    access_token=json_response["access_token"],
                    refresh_token=json_response["refresh_token"],
                    user=request.user,
                    bank=happy_bank,
                )

        return redirect("profile")


def logout(request):
    auth.logout(request)
    return redirect("home")

