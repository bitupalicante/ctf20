# Fintechno

## Puntos

`?`

## Pista

Ordenadas de menor a mayor coste de puntuación. (No añadir si no se va a restar puntuación por cada una de ellas):

* A veces el pasado nos persigue.
* Tal vez no necesites una cuenta en todas partes.
* ¿Autorización o autenticación?
* Es mejor reutilizar que reciclar.

## Adjuntos

* [a_sad_song.mp3](files/a_sad_song.mp3) (MD5: bf3c8295ad256e73a5c616630a3edc0b)

## Descripcion

Creo que he encontrado una canción de un supuesto leak pero no se reproducirla.

* [a_sad_song.mp3](files/a_sad_song.mp3) (MD5: bf3c8295ad256e73a5c616630a3edc0b)

## Flag

- bitup20{R3us34uth0r1z4t10nT0k3nsIs4b4dId34}	

## Deploy

Se deben modificar las siguientes variables de entorno del docker-compose

Se debe respetar el formato de las URLs sustituyendo únicamente el número del
puerto y el dominio/IP

- FINTECHNO_REDIRECT_URI=http://fintekno.bitupalicante.com/grants/
- HAPPY_BANK_URL=http://happybank.bitupalicante.com/

El fichero a_sad_song.mp3 de la carpeta files_to_upload debe ser accesible para
el usuario.

*** Importante!! ***
Por cada cambio en las variables de entorno se debe hacer un docker-compose build!!

Una vez modificadas las
- docker-compose build
- docker-compose up

## Write-Up

Fintechno explota la reutilización del token de autorización. 
El token de autorización en OAUTH debe invalidarse tras el primer uso. En caso
contrario, un atacante podría utilizarlo con diversos fines. En el CTF
tenemos a fintekno que es una APP que permite visualizar transacciones de distintos
bancos y HappyBank, que es un banco con el que se integra.

Se cree que se han filtrado datos de HappyBank y se señala a un fichero encontrado
en la red. `a_sad_song.mp3`.

Este fichero si lo analizamos veremos que es un fichero en texto plano, concretamente
un base64.

![Descubriendo el fichero sospechoso](photos/a_sad_song_is_a_sqlite.png)

Después de comprobar el tipo del fichero una vez decodeado el base64 observamos
que se trata de un fichero sqlite.

Al abrirlo con una herramienta veremos que en realidad se trata de un historial
de navegación.

![Abriendo el SQlite](photos/opening_sqlite.png)

Un historial de navegación de Firefox. Si realizamos una consulta a la tabla
moz_places veremos las URLs del historial del usuario.

![Historial de navegación](photos/is_a_firefox_history.png)

Si leemos detalladamente el historial podremos detectar que se trata de un usuario
que concede permisos a la aplicación fintechno en finbank y este devuelve un token
de autorización.

![Grant code](photos/grant_code.png)

Desde Fintechno veremos que podemos crearnos un usuario.

![Fintechno landing page](photos/fintechno_1.png)

![Fintechno registro](photos/registration_in_fintechno.png)

Una vez registrado el usuario nos dirá si lo queremos conectar con HappyBank.

![Conexión con HappyBank](photos/fintechno_profile.png)

Para conectarlo el usuario en el caso de uso normal iniciaría sesión con su
cuenta de HappyBank, previo a su registro, aceptaría permisos y después sería
redirigido a fintechno con el token de autorización de su cuenta de HappyBank.

En cambio para superar el reto se deberá utilizar el token obtenido en la URL
del historial. De esta forma se asociará la cuenta del usuario del historial
filtrado de HappyBank al usuario atacante creado en fintechno y podrá ver
sus transacciones.

![Flag](photos/flag_of_admin_user.png)


## Referencias

* http://blog.intothesymmetry.com/2015/12/top-10-oauth-2-implementation.html
