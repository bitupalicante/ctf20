# Deploy

Verificar el hash del export del contenedor (tienes el hash MD5 [aquí](../README.md)):

`md5sum waf.zip`

Cargar la imagen del contenedor con: 

`sudo docker load -i waf.zip`

Levantar el contenedor con:

`sudo docker run -d -p 61337:80 waf`

Y entonces puedes hacer una peticion a localhost:61337 para verificar que esta funcionando:

`curl -v http://127.0.0.1:61337`
