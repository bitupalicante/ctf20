# WAFXtreMe

## Puntos

* Puntuación Máxima `400`
* Puntuación Mínima `300`
* Decadencia `5`

## Pista

* `Se permite usar ls`

## Flag

`bitupCTF2020{TheWafIsD0wnWithTh1sFlag}`

## Adjuntos

None

## Deploy

* [waf.zip](deploy/waf.zip) (MD5: a941411ef36dd436d080b62e2764a173)

Recuerda que puedes encontrar todos los archivos del despliegue en [deploy](deploy).

### Requerimientos

Se requiere tener instalado **Docker** para poder desplegar el reto.

## Descripcion

El sysadmin estaba escribiendo un flag secreto en un editor emacs, pero algo le tuvo que pasar que le dió a unas teclas y no sabe ni dónde lo puso aunque piensa que lo mismo lo apuntó en el fichero de texto sobre el que se encontraba trabajando, o creó un fichero nuevo, un directorio, etc... ni lo sabe ni puede recordarlo debido a esas malditas combinaciones de teclas.

El problema que ha surgido, es que cambió todas las claves de acceso a la máquina sobre la que estuvo trabajando y la tuvo que enviar a la Antártida protegida por el WAF que había ideado, proyecto en el que se encontraba trabajando en aquellos momentos y que era considerado de alta seguridad por lo que nadie podría acceder a nada sin dicho flag, y con el COVID-19 tampoco nadie podría desplazarse a aquellos recónditos lugares.

Pero recordó que había dejado, para el control de la máquina, una puerta trasera que permitía al menos ejecutar algún comando que otro de forma remota en ella...

Si puedes ayudarle a descubrir el flag, el INC (Instituto Nacional de Chalados) te premiaría con XXXX puntos para el CTF de Bitup 2020.

https://wafxtreme.bitupalicante.com

P.D: el formato flag para este reto es: `bitupCTF2020{xxxx}`

## Solucion

Find (dirbuster, dirsearch, etc...)
`waf01/index.php`
`ls $sdofhsdhjs/???$osdihdhsdj`

flag: 
`bitupCTF2020{TheWafIsD0wnWithTh1sFlag}`

Where? 
`mkdir -p /var/bitupCTF2020{TheWafIsD0wnWithTh1sFlag}`

## Referencias

None
