# Guess The Hash

## Puntos

* Puntuación Máxima `100`
* Puntuación Mínima `50`
* Decadencia `10`

## Pista

None

## Flag

`bitup20{cb51f05cb20cd0fb81b09c1cc09b7ea3}`

## Adjuntos

* [files/GuessTheHash.rar](files/GuessTheHash.rar) (MD5: 814497f73e517af7da8badadeb120915)

## Deploy

None

## Descripcion

Estos puñeteros de Microsoft siempre andan ocultando cosas, menuda la que han liado con la flag.

## Solucion

1. Descargamos el archivo “Forensics.rar” en un sistema **Windows** (como nos sugiere la pista de la descripción) y lo descomprimimos.

![](capturas/1.png)

2. Eliminamos las propiedades de los archivos ocultos con `attrib -h -r -s /s /d *` y aparece un archivo con el siguiente contenido:

![](capturas/2.png)

3. Haciendo `dir /r` podemos ver un archivo **Zone Identifier**, un ADS (Alternate Data Stream) el cual contiene la procedencia del archivo hashes.txt, para ello lo abrimos con notepad:

![](capturas/3.png)

4. Descargamos el archivo de dicha web, la contraseña es la que encontramos en el archivo oculto info.txt localizado previamente.

![](capturas/4.png)

5. Ahora tenemos dos archivos de hashes, el que venía en el .rar y el que descargamos del navegador, y tienen un tamaño diferente:

![](capturas/5.png)

6. Haciendo un `diff` entre ambos archivos, en mi caso utilizando un Debian virtualizado, encontramos la línea que se ha modificado, esta es la flag correcta para resolver el reto:

![](capturas/6.png)

## Referencias

* https://es.wikipedia.org/wiki/Alternate_Data_Streams
