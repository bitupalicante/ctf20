# Deploy

Construir el contenedor con: 

`sudo docker build -t "comming-home" .`

Levantar el contenedor con:

`sudo docker run -d -p "127.0.0.1:8000:5000" --env THREADS=10 --env FLAG="bitup20{ni_pr0xy_ni_pr0sis}" -h "comming-home" --name="comming-home" comming-home`

Y entonces puedes conectarte al puerto 8000 para verificar que esta funcionando:

`curl http://127.0.0.1:8000`
